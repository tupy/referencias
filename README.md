# TuPy - Referências

## Sobre
### O que é o repositório Referências

Esse repositório objetiva reunir informações de importância para as atividades
do TuPy, time/grupo de estudos dedicado a maratonas de programação utilizando a
linguagem Python.

O repositório coletará informações sobre materiais e livros de interesse,
cursos, competições de programação, repositório de outros times, sites com lista
de problemas típicos encontrados em maratonas, e tudo mais que for relevante
para o TuPy.

### Como colaborar

Inclua sua colaboração escrita em [Markdown](https://docs.gitlab.com/ee/user/markdown.html)
nos tópicos específicos. Coloque um título de terceiro nível (usando `###`) e o
link para o conteúdo já no título. Em seguida, escreva uma descrição sobre o
conteúdo que está adicionando.

Mantenha cada linha do código fonte com no máximo 80 caracteres, exceto quando
utilizar links que podem ultrapassar esse limite.

Sinta-se a vontade para enviar suas contribuições diretamente para o
repositório, mas caso esteja com alguma dúvida abra merge requests. Assim vamos
treinando revisão de código por times de desenvolvedores.

## Livros e Apostilas

### [Pense em Python](https://penseallen.github.io/PensePython2e/)

Tradução para português do premiado livro "*Think Python*", é uma excelente porta
de entrada para aprender o básico da linguagem de programação.

### [Como Pensar Como um Cientista da Computação](https://panda.ime.usp.br/pensepy/static/pensepy/)

Tradução do Livro "*How to Think Like a Computer Scientist: Interactive
Version*", de Brad Miller e David Ranum, é um ótimo material para quem quer
aprender a sintaxe da linguagem Python, a fim de resolver problemas comuns ou
não tão comuns de computação.

### [Problem Solving with Algorithms and Data Structures using Python](http://interactivepython.org/runestone/static/pythonds/index.html)

Livro iterativo sobre estrutura de dados e algoritmos utilizando Python como
linguagem padrão. Há muita discussão e exemplos de implementação das principais
estruturas de dados (como pilhas, filas e árvores). Também é possível escrever e
executar códigos Python diretamente dos exemplos. É possível também baixar uma
[versão em PDF do livro](https://www.cs.auckland.ac.nz/courses/compsci105ssc/resources/ProblemSolvingwithAlgorithmsandDataStructures.pdf).

### [Data Structures and Algorithms in Python](http://multimedia.ucc.ie/Public/training/cycle1/algorithms-in-python.pdf)

Livro muito mais denso sobre algoritmos e estruturas de dados utilizando Python.
Há capítulos sobre sintaxe de Python e em seguida o fluxo básico de livros de
algoritmos como complexidade, análise assintótica, e estruturas de dados dos
mais variados tipos.

### [Competitive Programming](https://www.dropbox.com/s/fb68xw72xznv0p3/Competitive%20Programming%203.pdf?dl=0)

Um livro com dicas relevantes sobre programação, estruturas de dados e 
algoritmos para participantes de competições de programação em geral. Boa parte
dos códigos são desenvolvidos em C++ e Java. É possível baixar exemplos de
códigos e outros materiais na [página web](https://cpbook.net/) do livro.

### [Programming Challenges](http://acm.cs.buap.mx/downloads/Programming_Challenges.pdf)

Livro voltado às competições de programação, apresenta assuntos por tópicos como
estruturas de dados, strings, ordenação, programação dinâmica, e mais, e em
seguida uma lista de problemas relacionados. Os algoritmos no livro são escritos
em C++ e Java. Há um [website](http://www.programming-challenges.com/) com mais
informações e downloads.

## Cursos

### [Python para Zumbis](https://www.pycursos.com/python-para-zumbis/)

Curso grátis de introdução a Python mas também com conteúdo avançado
apresentando a versatilidade da linguagem. Ministrado por [Fernando Masanori](https://about.me/fmasanori)
, reconhecido membro da comunidade Python conhecido por sua dedicação ao ensino
da linguagem.

### [Introdução à Ciência da Computação com Python - Parte 1](https://pt.coursera.org/learn/ciencia-computacao-python-conceitos)

Apesar do nome, esse curso é mais sobre o aprendizado de Python em si.
Ministrada pelo professor da USP Fabio Kon com conteúdo produzido por membros do
[Centro de Competência em Software Livre da USP](http://ccsl.ime.usp.br/).

### [Introdução à Ciência da Computação com Python - Parte 2](https://pt.coursera.org/learn/ciencia-computacao-python-conceitos-2)

Continuando o curso anterior, dessa vez a parte de Python finaliza com temas
sobre matrizes, strings e programação orientada a objetos. Em seguida tem-se os
assuntos mais propriamente relacionados com ciência da computação, como
complexidade, algoritmos de busca e ordenação, recursão, e mais.

## Competições

### [Maratona de Programação da SBC](http://maratona.ime.usp.br/)

A mais prestigiada maratona de programação do país. Organizada pela SBC, é parte
das etapas sulamericanas da maratona internacional *ACM International Collegiate
Programming Contest* (ICPC). Equipes das mais diferentes universidades do Brasil
competem entre si na primeira fase em diversas sedes espalhadas pelo país. Em
seguida, as melhores participam presencialmente da final. A equipe vencedora
é então indicada para a final internacional do ICPC.

### [ACM International Collegiate Programming Contest](https://icpc.baylor.edu/)

Popularmente conhecida como ICPC, é a principal maratona de programação e a mais
conhecida no meio acadêmico. Organizada pela ACM, reúne competidores do mundo
inteiro após terem passado por etapas locais anteriores. Para competidores
brasileiros, vencer a *Maratona de Programação da SBC* é requisito para
participar do ICPC.

### [Google Code Jam](https://code.google.com/codejam/)

Competição internacional administrada pelo Google, consiste em uma série de
desafios algorítmicos que devem ser solucionados pelo competidor, de forma
individual, utilizando qualquer linguagem de programação. Com uma primeira etapa
realizada *online*, a final acontece presencialmente em algum dos escritórios
do Google.

### [Facebook Hacker Cup](https://www.facebook.com/hackercup)

Competição internacional administrada pelo Facebook, consiste em uma série de
desafios algorítmicos divididos em diferentes etapas, que devem ser solucionados
pelo competidor, de forma individual, utilizando qualquer linguagem de
programação. As primeiras etapas são realizadas via *online*, com as últimas
ocorrendo presencialmente em escritórios da empresa.
